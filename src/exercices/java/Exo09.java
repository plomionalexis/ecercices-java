/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercices;

import java.util.Scanner;


public class Exo09
{

    
    public static void main(String[] args)
    {
      
          // Déclaration et création d'un dispositif de saisie
        // désigné par la variable clavier
        Scanner clavier= new Scanner(System.in);
        
        // Décaration des variables d'entrée
        float note;
        
        // Déclaration et initialisation à 0 des variables de travail
        float total=0;
        int   nbNotes=0;
        float noteMax=0,noteMin=20;
   
        // Déclaration des variables de sortie    
        float moyenne;
        
        // On passe une ligne dans la console d'affichage
        System.out.println();
        
        //<editor-fold defaultstate="collapsed" desc="SAISIE D'UNE PREMIERE NOTE">
        
        // Saisie d'une note dans la variable note via le clavier
        System.out.println("Entrez une note: (note négative pour arrêter)");
        note=clavier.nextFloat();
        
        //</editor-fold>
    
        // Boucle TantQue ( le test entre parenthèses va être effectué une ou plusieurs fois) 
        // Si note n'est pas égale à -1 les instructions situées entre les deux accolades 
        // ( entre  lignes 41 et 60 dans ce programme) sont exécutées 
        // Si ce n'est pas le cas l'exécution se poursuit après l'accolade fermante du tantque
        // c'est à dire après la ligne 60 dans ce programme 
        while( note >=0  )
        {
            //<editor-fold defaultstate="collapsed" desc="TRAITEMENT DE LA NOTE">
            
            total= total+note; // On ajoute la note saisie au total
            nbNotes++;         // nbNote est augmenté de 1 ( incrémentation )
            
            //</editor-fold>
           
            //<editor-fold defaultstate="collapsed" desc="SAISIE D'UNE NOUVELLE NOTE">
            
            // Saisie d'une note dans la variable note via le clavier
            System.out.println("Entrez une note: (note négative pour arrêter)");
            
            
            //</editor-fold>
            
            if(note >=0)
            {    
                if(note<noteMin)
                {
                    noteMin=note;

                }
                
                if(note>noteMax)
                {
                 noteMax=note;   
                }
              
            }
            // Toujours mettre l'entré en fin de boucle
            note=clavier.nextFloat();
            
            
            // toutes les instructions situées entre les accolades du tantque ( lignes 41 et 60)
            // ont été exécutées
            // LE TEST DE LA LIGNE  40(dans la ligne while)  est A NOUVEAU SYSTEMATIQUEMENT EFFECTUE 
        } 
    
        // On teste si on a saisi au moins une note c'est à dire si nbNotes>0
        if ( nbNotes>0)
        {
           
           // si le test réussit on calcule la moyenne
           moyenne= total/nbNotes;
           
           // Affichage formaté de la moyenne (4 chiffres dont 2 après la virgule)
           System.out.printf("\nLa Moyenne est de: %4.2f\n",moyenne);
           System.out.println("La note la plus faible est :"+noteMin);
           System.out.println("La note la plus haute est :"+noteMax);
        }
        else{
            
           // On affiche ce message si on a saisi aucune note 
           System.out.println("\nVous n'avez saisi aucune note!");        
        }   
        
        // On passe une ligne dans la console d'affichage
        System.out.println();
        
        
        
    }
}
